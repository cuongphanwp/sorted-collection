package com.david;

import java.util.Map;

public class Main {
    private static StockList stockList = new StockList();

    public static void main(String[] args) {
        StockItem temp = new StockItem("bread", 0.86, 100);
        stockList.addStock(temp);

        temp = new StockItem("phone", 1000.0, 20);
        stockList.addStock(temp);
        temp = new StockItem("cake", 3.00, 7);
        stockList.addStock(temp);
        temp = new StockItem("coke", 0.90, 20);
        stockList.addStock(temp);
        temp = new StockItem("apple", 0.30, 30);
        stockList.addStock(temp);
        temp = new StockItem("banana", 2.00, 10);
        stockList.addStock(temp);
        temp = new StockItem("cup", 0.5, 200);
        stockList.addStock(temp);
        temp = new StockItem("cup", 0.45, 7);
        stockList.addStock(temp);


        temp = new StockItem("chair", 62.0, 10);
        stockList.addStock(temp);
        temp = new StockItem("door", 75.0, 4);
        stockList.addStock(temp);
        temp = new StockItem("milk", 2.20, 50);
        stockList.addStock(temp);

        System.out.println(stockList);

//        for (String s : stockList.Items().keySet()) {
//            System.out.println(s);
//        }

        Basket davidBasket = new Basket("David");
        sellItem(davidBasket, "door", 2);
        sellItem(davidBasket, "phone", 2);
        sellItem(davidBasket, "chair", 6);
        sellItem(davidBasket, "chair", 4);
        sellItem(davidBasket, "chair", 2);
        sellItem(davidBasket, "laptop", 4);
        sellItem(davidBasket, "milk", 20);

        removeItem(davidBasket,"door",2);
        removeItem(davidBasket,"chair",5);
        removeItem(davidBasket,"door",1);
//        System.out.println(davidBasket);

//        System.out.println(stockList);


        //Error so need to fix as below
        StockItem door = stockList.Items().get("door");
        if(door != null){
        door.adjustStock(2000);
        }
        if(door != null) {
            door.adjustStock(-2000);
        }
//        System.out.println(stockList);

        Basket basket = new Basket("customer");
        sellItem(basket,"cup",10);
        sellItem(basket,"coke",5);
        removeItem(basket,"cup",2);
        System.out.println(basket);


        System.out.println("Doors removed: " + removeItem(davidBasket,"door",1));
        System.out.println(davidBasket);

        System.out.println("\nDisplay stock list before and after checkout");
        System.out.println(basket);
        System.out.println(stockList);
        checkOut(basket);
        System.out.println(basket);
        System.out.println(stockList);

        checkOut(davidBasket);
        System.out.println(davidBasket  );
        System.out.println(stockList);

        for (Map.Entry<String, Double> price : stockList.priceList().entrySet()) {
            System.out.println(price.getKey() + " costs  " + price.getValue());
        }
    }

    public static int sellItem(Basket basket, String item, int quantity) {
        //retrieve item from stock list
        StockItem stockItem = stockList.get(item);
        if (stockItem == null) {
            System.out.println("We don't sell " + item);
            return 0;
        }
        if (stockList.reserveStock(item, quantity) != 0) {
            return basket.addToBasket(stockItem, quantity);
        }
        return 0;
    }

    public static int removeItem(Basket basket, String item, int quantity) {
        //retrieve item from stock list
        StockItem stockItem = stockList.get(item);
        if (stockItem == null) {
            System.out.println("We don't sell " + item);
            return 0;
        }
        if (basket.removeFromBasket(stockItem, quantity) == quantity) {
            return stockList.unreserveStock(item, quantity);
        }
        return 0;
    }

    public static void checkOut(Basket basket) {
        for (Map.Entry<StockItem, Integer> item : basket.Items().entrySet()) {
            stockList.sellStock(item.getKey().getName(), item.getValue());
        }
        basket.clearBasket();
    }
}
