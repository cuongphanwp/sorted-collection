package com.david;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;


public class StockList {
    private final Map<String, StockItem> list;

    public StockList() {
        this.list = new TreeMap<>();
    }

    public int addStock(StockItem item) {
        if (item != null) {
            //check if we already have quantities
            StockItem inStock = list.getOrDefault(item.getName(), item);
            //if there is stock, adjuct quantity
            if (inStock != item) {
                item.adjustStock(inStock.availableInStock());
            }

            list.put(item.getName(), item);
            return item.availableInStock();
        }
        return 0;
    }

    public int sellStock(String item, int quantity) {
        StockItem inStock = list.get(item);
        if((inStock != null) && (quantity > 0)){
            return inStock.finaliseStock(quantity);
        }
        return 0;

//        StockItem inStock = list.getOrDefault(item, null);
//
//        if ((inStock != null) && (inStock.availableInStock() >= quantity) && quantity > 0) {
//            inStock.adjustStock(-quantity);
//            return quantity;
//        }
//
//        return 0;
    }

    public int reserveStock(String item, int quantity){
        StockItem inStock = list.get(item);
        if(inStock != null && quantity > 0){
            return inStock.reserveStocks(quantity);
        }
        return 0;
    }

    public int unreserveStock(String item, int quantity){
        StockItem inStock = list.get(item);
        if(inStock != null && quantity > 0){
            return inStock.unreserveStocks(quantity);
        }
        return 0;
    }

    public StockItem get(String key) {
        return list.get(key);
    }

    public Map<String,Double> priceList(){
        Map<String, Double> prices = new TreeMap<>();
        for(Map.Entry<String,StockItem> item : list.entrySet()){
            prices.put(item.getKey(),item.getValue().getPrice());
        }
        return Collections.unmodifiableMap(prices);
    }


    public Map<String, StockItem> Items() {
        return Collections.unmodifiableMap(list);
    }

    @Override
    public String toString() {
        String s = "\nStock list\n";
        double totalCost = 0.0;
        for (Map.Entry<String, StockItem> item : list.entrySet()) {
            StockItem stockItem = item.getValue();

            double itemValue = stockItem.getPrice() * stockItem.availableInStock();

            s = s + stockItem + ". There are " + stockItem.availableInStock() + " in stock. Value of items ";
            s = s + "$" + String.format("%.2f", itemValue) + "\n";
            totalCost += itemValue;
        }

        return s + "Total stock value: $" + String.format("%.2f", totalCost);
    }
}
